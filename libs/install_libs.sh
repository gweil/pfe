#!/bin/bash

cd FunctionalPlus
mkdir -p build && cd build
cmake ..
make && make install
cd ../..

cd eigen-git-mirror
mkdir -p build && cd build
cmake ..
make && make install
ln -s /usr/local/include/eigen3/Eigen /usr/local/include/Eigen
cd ../..

cd json
mkdir -p build && cd build
cmake -DBUILD_TESTING=OFF ..
make && make install
cd ../..

cd frugally-deep
mkdir -p build && cd build
cmake ..
make && make install
cd ../..
