#!/bin/bash

git clone https://github.com/Dobiasd/FunctionalPlus
git clone -b '3.3.5' --single-branch --depth 1 https://github.com/eigenteam/eigen-git-mirror
git clone -b 'v3.7.1' --single-branch --depth 1 https://github.com/nlohmann/json
git clone -n https://github.com/Dobiasd/frugally-deep
cd frugally-deep
git checkout dbaad7a9a3711f5fd7f6c54f4274fbcd4987086f
cd ..
