import tensorflow.keras as keras
import numpy as np

from dataset import *
from loss import custom_loss
from tensorflow.keras.callbacks import ModelCheckpoint, CSVLogger
from scipy.io import wavfile

class Model():
    lstm_unit = 16

    def __init__(self, batch_size = 40, epochs = 100, dataset = None):
        self.batch_size = batch_size
        self.epochs = epochs
        self.dataset = dataset

        self.model = None
        self.history = None

        if (dataset != None):
            (x_train, y_train), (x_test, y_test) = self.dataset.formatDataset()

        self.x_train = x_train
        self.y_train = y_train
        self.x_test = x_test
        self.y_test = y_test

        self.__defineModel()

    def __defineModel(self):
        self.model = keras.Sequential()
        self.model.add(keras.layers.LSTM(self.lstm_unit, input_shape=(2048, 1),
                                         return_sequences=True))
        self.model.add(keras.layers.TimeDistributed(keras.layers.Dense(1)))

        self.model.compile(loss=custom_loss,
                           optimizer=keras.optimizers.Adam(learning_rate=0.001),
                           metrics=None)
        
    def summary(self):
        print(self.model.summary())

    def train(self, verbose = 1):
        cb = [ModelCheckpoint('model_chkpt.hdf5', monitor='val_loss', save_best_only=True),
              CSVLogger('./training_csv.csv')]
        
        self.history = self.model.fit(self.x_train, self.y_train,
                                      epochs = self.epochs,
                                      shuffle = True,
                                      batch_size = self.batch_size,
                                      verbose = verbose,
                                      validation_split = 0.2,
                                      callbacks = cb)
            
    def save(self, path):
        self.model.save(path)
        model_json = self.model.to_json()
        with open("fd.json", "w") as json_file:
            json_file.write(model_json)

    def evaluate(self, frame, verbose = 1):
        result = self.model.predict(frame,
                                    batch_size = self.batch_size,
                                    verbose = verbose)
        return result
    
if __name__ == '__main__':
    dataset = Dataset('data/guitar_default.wav', 'data/guitar_gain_10.wav', duration=120)

    # Specify the desired number of epochs
    model_guitar = Model(epochs = 1, dataset = dataset)
    
    model_guitar.train()
    model_guitar.save('model.hdf5')
    
    y_test = model_guitar.y_test
    y_pred = model_guitar.evaluate(y_test)
    y_pred = y_pred.reshape((np.shape(y_pred)[0] * np.shape(y_pred)[1], 1))
    wavfile.write('result.wav', 44100, y_pred)
