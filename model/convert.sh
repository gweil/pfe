#!/bin/bash

fdeepPath=/builds/gweil/pfe/libs/frugally-deep/

convertPath=${fdeepPath}keras_export/convert_model.py
python3 $convertPath $1 $2
