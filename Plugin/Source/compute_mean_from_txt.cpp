#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <sstream>

/*-----------------------------------------------------
 * Author : mleveque 17-03-2020
 * This program is an independant script which computes
 * the mean from a file filled with floats in the form
 * of one float per line.
 * Since it is independant, this script has to be compiled
 * using g++.
 * Full command : g++ compute_mean_from_txt.cpp -std=c++11 -Wall -Wextra -o compute_mean_from_txt
 * ---------------------------------------------------*/

using namespace std;

int main(int argc, char *argv[]) {
  if(argc != 2)
    return -1;
  int number_of_lines = 0;
  string line;
  string filename(argv[1]);

  ifstream myfile(filename);
  if (!myfile.is_open())
  {
    cout << "Your file did not open correctly." << endl;
    return -1;
  }
  float values_total = 0.0f;

  while (getline(myfile, line))
  {
    istringstream in(line);
    float val;
    in >> val;
    values_total+=val;
    ++number_of_lines;
  }
  cout << "Result : " << values_total/number_of_lines << "ms." << endl;
  return 0;
}
