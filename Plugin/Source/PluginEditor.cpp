/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
PluginAudioProcessorEditor::PluginAudioProcessorEditor (PluginAudioProcessor& p)
    : AudioProcessorEditor (&p), processor (p)
{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (WINDOW_WIDTH, WINDOW_HEIGHT);
    addAndMakeVisible (gainSlider);
    addAndMakeVisible (volumeSlider);
    addAndMakeVisible (switch_button);
    
    int range[2] = {RANGE_MIN,RANGE_MAX};
    
    //volumeSlider parameters setup
    volumeSlider.setRange (range[0], range[1]);
    volumeSlider.setSliderStyle (Slider::Rotary);
    volumeSlider.setTextBoxStyle (Slider::TextEntryBoxPosition::TextBoxBelow, false, 60, 10);

    float default_value = 50.0f;
    volumeSlider.setValue(default_value);
    
    //add listener to the slider
    volumeSlider.addListener (this);
    volumeSlider.setNumDecimalPlacesToDisplay(1);
    
    //gainSlider parameters setup
    gainSlider.setRange(range[0], range[1]);
    gainSlider.setSliderStyle (Slider::Rotary);
    gainSlider.setTextBoxStyle (Slider::TextEntryBoxPosition::TextBoxBelow, false, 60, 10);

    gainSlider.setValue(default_value);
    gainSlider.addListener (this);
    gainSlider.setNumDecimalPlacesToDisplay(1);
    
    //Button Switch
    switch_button.setButtonText("TURN ON");
    switch_button.addListener(this);
    
}

PluginAudioProcessorEditor::~PluginAudioProcessorEditor()
{
}

//==============================================================================
void PluginAudioProcessorEditor::paint (Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));

    g.setColour (Colours::white);
    float font_size = 15.0f;
    g.setFont (font_size);
    
    int offset_text_X = 50;
    int offset_text_Y = 70;
    
    int text_width = 150;
    int text_height = 50;
    
    Rectangle<int> gainPosition = Rectangle<int>(gainSlider.getX()-offset_text_X,
                                                 gainSlider.getY()+offset_text_Y,
                                                 text_width,
                                                 text_height);
    Rectangle<int> volumePosition = Rectangle<int>(volumeSlider.getX()-offset_text_X,
                                                   volumeSlider.getY()+offset_text_Y,
                                                   text_width,
                                                   text_height);
    g.drawText ("LEVEL", volumePosition , Justification::centred, 1);
    g.drawText ("GAIN", gainPosition, Justification::centred,1);
    

}

void PluginAudioProcessorEditor::resized()
{
    // This is generally where you'll want to lay out the positions of any
    // subcomponents in your editor..
    int height_sliders = 50;
    int size_sliders_X = 50;
    int size_sliders_Y = 85;
    
    int offset_left = 50;
    gainSlider.setBounds(offset_left, height_sliders, size_sliders_X, size_sliders_Y);
    
    int offset_right = offset_left + size_sliders_X;
    volumeSlider.setBounds(WINDOW_WIDTH - offset_right, height_sliders, size_sliders_X, size_sliders_Y);

    
    int size_X = 100;
    int size_Y = 30; //arbitrary values
    int offset_button_X = size_X/2;
    int switch_X = WINDOW_WIDTH / 2 - offset_button_X;
    int switch_Y = 3 * (WINDOW_HEIGHT / 4);
    
    Rectangle<int> switch_position = Rectangle<int>(switch_X,
                                                    switch_Y,
                                                    size_X,
                                                    size_Y);
    switch_button.setBounds(switch_position);
    
}

void PluginAudioProcessorEditor::sliderValueChanged(Slider* slider)
{
    if (slider == &gainSlider)
        processor.setStrength(slider->getValue());
    
    //Volume Slider informations
    if (slider == &volumeSlider)
    {
        processor.setVolume(slider->getValue());
    }
}

void PluginAudioProcessorEditor::buttonClicked(Button* button)
{
    if (button == &switch_button)
    {
        if(button->getToggleState()){
            button->setButtonText("TURN OFF");
            processor.setEffectToggled(true);
        }
        else{
            button->setButtonText("TURN ON");
            processor.setEffectToggled(false);
        }
    }
}
