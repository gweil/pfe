#!/bin/bash

model_path="./include/model.h"
end="\";"
if [ ! -d include ]; then
  mkdir include
fi
if [ -f $model_path ]; then
  rm $model_path
fi
echo "#include <string.h>" >> $model_path
printf "std::string " >> $model_path
printf "str_model = \"" >> $model_path

while read line
do
  printf "${line//\"/\\\\\"}" >> $model_path
done

printf $end >> $model_path
