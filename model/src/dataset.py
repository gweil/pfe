import numpy as np
from scipy.io import wavfile

file_path_guitar_default = "data/guitar_default.wav"
file_path_guitar_gain = "data/guitar_gain_10.wav"

class Dataset():
    debug = False

    def __init__(self, wavfile_default, wavfile_gain, frame_size = 2048, duration = 'full', train_ratio = 0.8):
        self.data = []
        self.data_default = []
        self.wavfile_default = wavfile_default
        self.wavfile_gain = wavfile_gain
        self.frame_size = frame_size
        self.duration = duration
        self.train_ratio = train_ratio
        self.__loadWav()

    # Return a formated numpy array (nb_frame, frame_size, 1)
    def formatDataset(self):
        default = self.data_default
        gain = self.data

        # Convert to 1D numpy array
        default = np.stack(default, axis=0).flatten()
        gain = np.stack(gain, axis=0).flatten()
        default = default / 32768 # Absolute max value on a 16bits sample
        gain = gain / 32768       # Absolute max value on a 16bits sample
        
        # Cut data in 2 parts: training and test
        default_train, default_test = self.__cutData(default)
        gain_train, gain_test = self.__cutData(gain)

        # Reshape data according to frame_size
        default_train = self.__reshape(default_train)
        default_test = self.__reshape(default_test)
        gain_train = self.__reshape(gain_train)
        gain_test = self.__reshape(gain_test)

        return (default_train, gain_train), (default_test, gain_test)

    def __cutData(self, data):
        assert self.train_ratio <= 1
        train_lim = int(self.train_ratio * len(data))

        train = data[:train_lim]
        test = data[train_lim:]

        # Instead of adding zero (zero padding) we drop the last frame if it is not full
        train = train[0:int(len(train) // self.frame_size) * self.frame_size]
        test = test[0:int(len(test) // self.frame_size) * self.frame_size]
        
        return train, test

    def __reshape(self, data):
        data = data.reshape( (int(len(data) / self.frame_size), self.frame_size, 1) )
        return data

    def __loadWav(self):
        ### Default
        self.sample_rate, self.data_default = wavfile.read(self.wavfile_default)
        ### Gain
        _, self.data = wavfile.read(self.wavfile_gain)
        
        if self.duration != 'full':
            duration = int(self.duration)
            self.data_default = self.data_default[0:self.sample_rate * duration]
            self.data = self.data[0:self.sample_rate * duration]
            assert len(self.data_default) / self.sample_rate == duration
            assert len(self.data) / self.sample_rate == duration
        
        self.data_default = np.expand_dims(self.data_default, 1)
        self.data = np.expand_dims(self.data, 1)
