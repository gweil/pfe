/*
  ==============================================================================

    EffectNet.h
    Created: 19 Feb 2020 12:22:50am
    Author:  barcamille

  ==============================================================================
*/

#pragma once

#include <fdeep/fdeep.hpp>
#include <fdeep/model.hpp>
#include <JuceHeader.h>




class EffectNet
{
public:
    EffectNet();
    void processData(AudioBuffer<float>& buffer, bool effectToggled, bool perfTests);



private:
    fdeep::model model;

    int frameSize;
    fdeep::tensor_shape input_shape;
};
