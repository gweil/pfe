/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"

//==============================================================================
/**
*/
class PluginAudioProcessorEditor  : public AudioProcessorEditor,
                                    private Slider::Listener,
                                    private Button::Listener
{
public:
    PluginAudioProcessorEditor (PluginAudioProcessor&);
    ~PluginAudioProcessorEditor();

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;
    

private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    PluginAudioProcessor& processor;

    // GUI components
    Slider gainSlider;
    Slider volumeSlider;
    ToggleButton switch_button;
    
    
    const static int WINDOW_WIDTH = 400;
    const static int WINDOW_HEIGHT = 300;
    
    const static int RANGE_MIN = 0;
    const static int RANGE_MAX = 100;


    virtual void sliderValueChanged(Slider* slider) override;
    virtual void buttonClicked(Button* button) override;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (PluginAudioProcessorEditor)
};
