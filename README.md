# PFE - Audio Synthesis of Distortion in Real-Time

University project based on this paper : https://research.aalto.fi/files/36768418/ELEC_Wright_Real_time_black_box_DAFx2019.pdf

---
---

The Jupyter notebook in 'model/notebook/' is the same code as the python code. It is there just for convenience if you plan to use it with Colab or just if you prefer to work on it.
