import sys
sys.path.append('src/')
from dataset import *

import unittest
import math
import numpy as np

class TestFormat(unittest.TestCase):
    def __init(self):
        super(TestFormat, self).__init__()
        
        sample_rate = 44100
        duration = 10

        #:INFO:Jeremi:1703:
        #   __new__() is used to prevent __init__() calling.
        #   And prevent __loadWav error (no file to load)
        #   member variables must be instantiated by hand.
        dataset = Dataset.__new__(Dataset)

        dataset.data = []
        dataset.data_default = []

        dataset.frame_size = 2048
        dataset.duration = duration
        dataset.train_ratio = 0.8
        dataset.normalize_data = False
        dataset.sample_rate = sample_rate

        dataset.wavfile_default = np.zeros((sample_rate * duration, 1))
        dataset.wavfiles_gain = np.zeros((sample_rate * duration, 1))

        return dataset

    def testCutData(self):
        dataset = self.__init()
        train, test = dataset._Dataset__cutData(dataset.wavfile_default)
        
        #:INFO:Jeremi:1703:
        #   As train ~ 0.5, we have to round up for the assertEqual 
        #   Procedure:
        #     0.49 * 10 = 4.9999
        #     => ceil(4.9) = 5
        #     5 / 10 = 0.5
        rounded_train = math.ceil(np.shape(train)[0] / len(dataset.wavfile_default) * 10) / 10
        rounded_test = math.ceil(np.shape(test)[0] / len(dataset.wavfile_default) * 10) / 10
        self.assertEqual(rounded_train, dataset.train_ratio)
        self.assertEqual(rounded_test, 0.2)
    
    def testCutDataEqualOne(self):
        dataset = self.__init()
        dataset.train_ratio = 1.0

        train, test = dataset._Dataset__cutData(dataset.wavfile_default)

        self.assertEqual(len(test), 0)

    def testCutDataGreaterOne(self):
        dataset = self.__init()
        dataset.train_ratio = 1.2
        with self.assertRaises(Exception):
            train, test = dataset._Dataset__cutData(dataset.wavfile_default)

    def testReshape(self):
        dataset = self.__init()
        dataset.train_ratio = 1.0

        train, test = dataset._Dataset__cutData(dataset.wavfile_default)
        data = dataset._Dataset__reshape(train)
        self.assertEqual(np.shape(data), (215, 2048, 1))

    def testLoad(self):
        pass
