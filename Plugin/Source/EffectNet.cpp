/*
  ==============================================================================

    EffectNet.cpp
    Created: 19 Feb 2020 12:22:50am
    Author:  barcamille

  ==============================================================================
*/

#include "EffectNet.h"
#include "../include/model.h"
#include <ctime>
#include <iostream>
#include <fstream>
#include <string>

EffectNet::EffectNet()
    : model(fdeep::read_model_from_string(str_model)),
      input_shape(1)
{
    frameSize = model.generate_dummy_inputs()[0].width();
    input_shape = model.generate_dummy_inputs()[0].shape();
}

std::string b_to_s(bool v)
{
    return v ? "true" : "false";
}

/*
 * count_lines is used for the performance tests on EffectNet.
 * The idea is to read the file named filename filled with times until 1,000 lines.
 */
int count_lines(std::string filename)
{
    int number_of_lines = 0;
    std::string line;
    std::ifstream myfile(filename);

    while (std::getline(myfile, line))
        ++number_of_lines;
    return number_of_lines;
}

/*
 * processData reads the Audio buffer values and applies a transformation.
 * effectToggled verifies if the effect button is on, if it is not, we skip the frugally-deep part to gain performances.
 * perfTests enables or not the performance tests. The idea of this test is to save the process time in a txt file.
 */
void EffectNet::processData(AudioBuffer<float>& buffer, bool effectToggled, bool perfTests)
{
    const clock_t c_start = std::clock();
    if(effectToggled)
    {
        std::vector<float> input_values;

        for (auto j = 0; j < buffer.getNumSamples(); ++j)
        {
            input_values.push_back(buffer.getReadPointer (0) [j]);
            if ((j+1) % frameSize == 0)
            {
                const fdeep::shared_float_vec sv(fplus::make_shared_ref<fdeep::float_vec>(std::move(input_values)));
                fdeep::tensor input(input_shape, sv);
                const auto result = model.predict({input});
                input_values = std::vector<float>();
                const std::vector<float> output = *(result[0]).as_vector();
                int startIndex = j -frameSize;
                for(int i = 0; i < frameSize; i++)
                    buffer.getWritePointer(0)[startIndex + i] = output[i];
            }
        }
        
    }
    if(perfTests)
    {
        const std::clock_t c_end = std::clock();
        float time = 1000.0 * (c_end-c_start) / CLOCKS_PER_SEC;
        std::ofstream myfile;
        std::string filename = "fs_"+std::to_string(buffer.getNumSamples())+"_"+b_to_s(effectToggled)+".txt";
        if (count_lines(filename) < 1000)
        {
            myfile.open (filename, std::ios_base::app);
            myfile << time << std::endl;
        }
        else std::cout << "1000 LINES !!! " << std::endl;
        
        std::cout << time << " milliseconds ellapsed for " << buffer.getNumSamples() << " samples." << std::endl;
    }
    
}
