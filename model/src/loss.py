import tensorflow as tf

def esr_backend(target, pred):
    loss = tf.math.reduce_sum(tf.math.square(tf.math.abs(tf.math.subtract(target, pred))))
    energy = tf.math.reduce_sum(tf.square(tf.math.abs(target))) + 0.000001
    return tf.math.divide(loss, energy)

def dc_backend(target, pred):
    loss = tf.math.square(tf.math.abs(tf.math.reduce_mean(tf.math.subtract(target, pred))))
    energy = tf.math.reduce_mean(tf.math.square(tf.math.abs(target))) + 0.000001
    return tf.math.divide(loss, energy)
    
#:COMMENT:mleveque:1102:Custom loss function based on the paper,
# the concatenation of ESR and DC, equation 16
def custom_loss(y_actual, y_predicted):
    ESR = esr_backend(y_actual, y_predicted)
    DC = dc_backend(y_actual, y_predicted)
    return tf.math.add(ESR, DC)