import sys
sys.path.append('src/')
from loss import *

import unittest
import numpy as np

class TestLoss(unittest.TestCase):
  t = 1
  fs = 44100

  def test_loss_equal(self):
      sin = np.sin(np.pi / 2.0 * 440 * np.linspace(0, self.t, int(self.fs * self.t)))
      tensor = tf.convert_to_tensor(sin)

      loss = round(custom_loss(tensor, tensor).numpy(), 1) 
      self.assertEqual(loss, 0.0)

  def test_loss_double(self):
      f = 440

      sin0 = np.sin(np.pi / 2.0 * f * np.linspace(0, self.t, int(self.fs * self.t)))
      tensor0 = tf.convert_to_tensor(sin0)

      sin1 = np.sin(np.pi / 4.0 * f * np.linspace(0, self.t, int(self.fs * self.t)))
      tensor1 = tf.convert_to_tensor(sin1)

      loss = round(custom_loss(tensor0, tensor1).numpy(), 1)
      self.assertEqual(loss, 2.0)
  
  def test_loss_rnd(self):

        sig0 = np.random.uniform(-1,1,self.fs * self.t)
        tensor0 = tf.convert_to_tensor(sig0)

        loss = round(custom_loss(tensor0, tensor0).numpy(), 1)
        self.assertEqual(loss, 0.0)
