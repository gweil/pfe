#!/bin/bash

if [[ ! -f model.hdf5 ]]; then
  cd ../model/
  python3 src/model.py
  mv fd.json ../Plugin/
  cd ../Plugin/
fi
./utils/model_to_c_string.sh < fd.json
